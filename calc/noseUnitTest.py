import add
import multiply
import sub
import divide



def test_multiply_3_4():
    assert multiply.multiply(3, 4) == 12

def test_multiply_4_a():
    assert multiply.multiply(4, 'a') == 'aaaa'

def test_multiply_a_3():
    assert multiply.multiply('a', 3) == 'aaa'
    
def test_add_5_3():
    assert add.add(5, 7) == 12
    
def test_add_withdecimal():
    assert add.add(3.7, 8.9) == 12.600000000000001
    
def test_sub_10_4():
    assert sub.sub(10, 4) == 6
    
def test_sub_3_7():
    assert sub.sub(3, 7) == -4
    
def test_divide_9_3():
    assert divide.divide(9, 3) == 3
    
def test_divide_2_4():
    assert divide.divide(2, 4) == 0
    